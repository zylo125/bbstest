create table comments (
  id integer primary key,
  user_name text,
  body text
);