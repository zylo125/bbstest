require 'sinatra'
require 'active_record'

ActiveRecord::Base.establish_connection(
  'adapter' => 'sqlite3',
  'database' => './bbs.db'
)

class Comment < ActiveRecord::Base
end

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end

get '/' do
  @comment = Comment.order('id desc').all
  erb :index
end

post '/new' do

  Comment.create({  :user_name => params[:user_name],
                    :body => params[:comment]
                  })
  redirect '/'
end